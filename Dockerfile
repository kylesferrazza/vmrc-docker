FROM kylesferrazza/aur:latest
ADD vmware-vmrc /vmware-vmrc
ADD vmware-keymaps /vmware-keymaps

ENV PKGEXT=.pkg.tar

RUN sudo pacman -Syu --needed --noconfirm noto-fonts

WORKDIR /vmware-keymaps
RUN sudo chown -R kyle:wheel . && makepkg -sri --noconfirm

ADD VMware-Remote-Console-11.2.0-16492666.x86_64.bundle /vmware-vmrc/
WORKDIR /vmware-vmrc
RUN sudo chown -R kyle:wheel . && makepkg -sri --noconfirm

ADD vmware.preferences /home/kyle/.vmware/preferences

ADD entry.sh /
ENTRYPOINT ["/entry.sh"]
