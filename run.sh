#! /usr/bin/env nix-shell
#! nix-shell -i bash -p xorg.xhost
xhost +local:root
docker run -it \
    --rm \
    --env="DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    vmrc $@
xhost -local:root
